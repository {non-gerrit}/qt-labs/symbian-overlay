# Copyright 1999-2010 Gentoo Foundation
# Copyright 2010 Nokia Corporation and/or its subsidiary(-ies)
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Symbian ADT Sourcery G++ Lite for ARM SymbianOS"
HOMEPAGE="http://www.codesourcery.com/sgpp/lite/arm/portal/release1258"

SRC_URI="http://www.codesourcery.com/sgpp/lite/arm/portal/package6321/arm-none-symbianelf/symbian-adt-4.4-172-arm-none-symbianelf.bin"
LICENSE="sourcery-g++"

RESTRICT="binchecks mirror strip"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

src_unpack() {
        mkdir ${S}
        cp ${DISTDIR}/${A} ${S}
}

src_compile() {
	true
}

src_install() {
        mkdir ${S}/bin
        cp -l /bin/* ${S}/bin
        cp -lr /lib* ${S}
        mkdir -p usr/bin
        cp -l /usr/bin/awk usr/bin
        cp -l /usr/bin/cksum usr/bin
        cp -l /usr/bin/which usr/bin
        cp -lr /usr/lib* ${S}/usr
        mkdir -p proc tmp
        cp -dr /dev .

        cp ${FILESDIR}/install-script.sh .
        chmod ugo+x install-script.sh

        # addwrite is for chroot to work.
        addwrite /
        einfo "Launching installer..."
        chroot ${S} /bin/bash install-script.sh || die "chroot failed"

        mkdir -p ${D}/usr
        cp -r usr/arm-none-symbianelf ${D}/usr

        einfo "Fixing file permissions..."
        find ${D}usr/arm-none-symbianelf -print0 | xargs -0 chmod o-w

        einfo "Adding symlinks..."
        mkdir ${D}usr/bin
        pushd ${D}usr/bin
        ln -s ../arm-none-symbianelf/bin/* .
        popd

        einfo "Removing unneeded files..."
        rm -rf ${D}usr/arm-none-symbianelf/[Uu]ninstall*
        rm -rf ${D}usr/arm-none-symbianelf/jre
}
