# Copyright 1999-2010 Gentoo Foundation
# Copyright 2010 Nokia Corporation and/or its subsidiary(-ies)
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Tool to automatically install and run applications on Symbian phones"
HOMEPAGE="http://qt.nokia.com/"

MY_PV=${PV/_/-}
SRC_URI="http://get.qt.nokia.com/qt/source/qt-everywhere-opensource-src-${MY_PV}.tar.gz"
LICENSE="LGPL-2.1 GPL-3"

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
DEPEND=">=x11-libs/qt-core-4.6.0"

src_compile() {
        cd qt-everywhere-opensource-src-${MY_PV}/tools/runonphone
	qmake || die "qmake failed"
        emake || die "emake failed"
}

src_install() {
        cd qt-everywhere-opensource-src-${MY_PV}/tools/runonphone
        exeinto /usr/bin
	doexe runonphone || die "install failed"
}
