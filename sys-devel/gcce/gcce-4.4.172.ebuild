# Copyright 1999-2010 Gentoo Foundation
# Copyright 2010 Nokia Corporation and/or its subsidiary(-ies)
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Symbian ADT Sourcery G++ Lite for ARM SymbianOS"
HOMEPAGE="http://www.codesourcery.com/sgpp/lite/arm/portal/release1258"

SRC_URI="http://www.codesourcery.com/sgpp/lite/arm/portal/package6321/arm-none-symbianelf/symbian-adt-4.4-172-arm-none-symbianelf.bin"
LICENSE="sourcery-g++"

RESTRICT="binchecks mirror strip"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

src_unpack() {
        mkdir ${S}
        cp ${DISTDIR}/${A} ${S}
}

src_compile() {
	true
}

src_install() {
        # Fake the home directory to avoid access violations.
        oldHOME=$HOME
        mkdir ${D}/temphome
        export HOME=${D}/temphome

        # It's tempting to use addpredict here to avoid actual writes, but the sandbox is
        # not able to stop the writes, so it's better to be honest, enable writes, and
        # just take precautions.
        addwrite /root
        if [ -f /root/.com.zerog.registry.xml ]; then
            cp /root/.com.zerog.registry.xml /root/.com.zerog.registry.xml.EBUILD-BACKUP
        fi

        chmod u+x ${A}
        einfo "Launching installer..."
        (
                # Lots of enters for the license
                for i in `seq 1 100`; do
                        echo
                done
                # The next sequence is:
                # License: y
                # Install type: Custom (3)
                # Deselect documentation: 2
                # Install path
                # Confirm install path: y
                # Make links: None (4)
                # Some final newlines
                cat <<EOF
y
3
2
${D}usr/arm-none-symbianelf
y
4



EOF
        ) | ./${A} -i console >/dev/null

        rm -f /root/.com.zerog.registry.xml
        if [ -f /root/.com.zerog.registry.xml.EBUILD-BACKUP ]; then
            mv /root/.com.zerog.registry.xml.EBUILD-BACKUP /root/.com.zerog.registry.xml
        fi

        export HOME=$oldHOME
        rm -rf ${D}/temphome

        einfo "Fixing file permissions..."
        find ${D}usr/arm-none-symbianelf -print0 | xargs -0 chmod o-w

        einfo "Adding symlinks..."
        mkdir ${D}usr/bin
        pushd ${D}usr/bin
        ln -s ../arm-none-symbianelf/bin/* .
        popd

        einfo "Removing unneeded files..."
        rm -rf ${D}usr/arm-none-symbianelf/[Uu]ninstall*
        rm -rf ${D}usr/arm-none-symbianelf/jre
}
