#!/bin/bash

(
        # Lots of enters for the license
        for i in `seq 1 100`; do
                echo
        done
        # The next sequence is:
        # License: y
        # Install type: Custom (3)
        # Deselect documentation: 2
        # Install path
        # Confirm install path: y
        # Make links: None (4)
        # Some final newlines
        cat <<EOF
y
3
2
/usr/arm-none-symbianelf
y
4



EOF
) | sh ./symbian-adt* -i console
