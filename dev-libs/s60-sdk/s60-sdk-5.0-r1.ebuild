# Copyright 1999-2010 Gentoo Foundation
# Copyright 2010 Nokia Corporation and/or its subsidiary(-ies)
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
inherit eutils

DESCRIPTION="Nokia S60 5th Edition SDK with Gnupoc patches"
HOMEPAGE="http://qt.nokia.com/"

SRC_URI="http://pepper.troll.no/s60prereleases/${P}.tar.bz2"
LICENSE="nokia-eula"

RESTRICT="binchecks mirror strip"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RDEPEND=">=app-emulation/wine-1.0"

src_prepare() {
	epatch "${FILESDIR}/Fixed-wrong-member-definition-for-GCCE-4.patch" || die "Could not apply patch"
}

src_compile() {
	true
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
        doenvd $FILESDIR/90s60-sdk
}
