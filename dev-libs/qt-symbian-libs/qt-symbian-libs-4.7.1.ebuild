# Copyright 1999-2010 Gentoo Foundation
# Copyright 2010 Nokia Corporation and/or its subsidiary(-ies)
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils qt4-build

DESCRIPTION="Symbian development libraries of Qt from Nokia"
HOMEPAGE="http://qt.nokia.com/"

SRC_URI="http://pepper.troll.no/s60prereleases/${P}.tar.bz2 http://get.qt.nokia.com/qt/source/qt-everywhere-opensource-src-4.7.1.tar.gz"
LICENSE="LGPL-2.1 GPL-3"

RESTRICT="binchecks"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
# We don't need the strict module checking that the Linux version of Qt does,
# since we install all module headers at once.
E_RDEPEND=""
E_IUSE=""
DEPEND=">=dev-libs/s60-sdk-5.0
        >=sys-devel/gcce-4.4.172
        app-mobilephone/runonphone"

QT4_EXTRACT_DIRECTORIES="src tools/shared/symbian tools/shared/windows"
QT4_TARGET_DIRECTORIES="src/tools"

QA_PRESTRIPPED="
                /usr/share/qt4/qt-symbian/lib/qmlparticlesplugin.dso
                /usr/share/qt4/qt-symbian/lib/QtScript{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qjpeg{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer_3_1{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtWebKit.dso
                /usr/share/qt4/qt-symbian/lib/qmlwebkitplugin{000a0000}.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer_3_2.dso
                /usr/share/qt4/qt-symbian/lib/qcncodecs.dso
                /usr/share/qt4/qt-symbian/lib/qmng.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer.dso
                /usr/share/qt4/qt-symbian/lib/QtMultimedia{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qico.dso
                /usr/share/qt4/qt-symbian/lib/QtXmlPatterns.dso
                /usr/share/qt4/qt-symbian/lib/QtSql.dso
                /usr/share/qt4/qt-symbian/lib/qmlfolderlistmodelplugin{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer_3_1.dso
                /usr/share/qt4/qt-symbian/lib/QtNetwork.dso
                /usr/share/qt4/qt-symbian/lib/qgif{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtCore.dso
                /usr/share/qt4/qt-symbian/lib/QtGui{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qcncodecs{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtNetwork{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qtiff.dso
                /usr/share/qt4/qt-symbian/lib/qsvgicon{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtMultimedia.dso
                /usr/share/qt4/qt-symbian/lib/qmlfolderlistmodelplugin.dso
                /usr/share/qt4/qt-symbian/lib/qkrcodecs{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qkrcodecs.dso
                /usr/share/qt4/qt-symbian/lib/QtDeclarative{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtGui.dso
                /usr/share/qt4/qt-symbian/lib/qmlgesturesplugin{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtXml{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtSvg{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsvg.dso
                /usr/share/qt4/qt-symbian/lib/qjpcodecs.dso
                /usr/share/qt4/qt-symbian/lib/qtwcodecs{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsvgicon.dso
                /usr/share/qt4/qt-symbian/lib/phonon.dso
                /usr/share/qt4/qt-symbian/lib/qico{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtScript.dso
                /usr/share/qt4/qt-symbian/lib/QtSql{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtXmlPatterns{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qmlgesturesplugin.dso
                /usr/share/qt4/qt-symbian/lib/qjpcodecs{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtDeclarative.dso
                /usr/share/qt4/qt-symbian/lib/QtTest{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtXml.dso
                /usr/share/qt4/qt-symbian/lib/phonon_mmf.dso
                /usr/share/qt4/qt-symbian/lib/qtracegraphicssystem.dso
                /usr/share/qt4/qt-symbian/lib/qjpeg.dso
                /usr/share/qt4/qt-symbian/lib/qmlwebkitplugin.dso
                /usr/share/qt4/qt-symbian/lib/QtCore{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qtwcodecs.dso
                /usr/share/qt4/qt-symbian/lib/qsvg{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qmng{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qgif.dso
                /usr/share/qt4/qt-symbian/lib/qtiff{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qmlparticlesplugin{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/phonon_mmf{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/QtWebKit{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qsymbianbearer_3_2{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/phonon{00040400}.dso
                /usr/share/qt4/qt-symbian/lib/qtracegraphicssystem{00040701}.dso
                /usr/share/qt4/qt-symbian/lib/qtmain.lib
                /usr/share/qt4/qt-symbian/lib/QtSvg.dso
                /usr/share/qt4/qt-symbian/lib/QtTest.dso"

src_unpack() {
    qt4-build_src_unpack
    unpack ${P}.tar.bz2
}

symbianqtenv() {
    setqtenv
    export PATH=$PATH:/usr/s60-sdk/epoc32/tools
    export EPOCROOT=/usr/s60-sdk
    export HOME=${T}/home
}

src_prepare() {
    mkdir -p ${T}/home
    symbianqtenv

    # Wine tries to open sound devices.
    addpredict /dev/snd

    # Generate wine config
    rcomp >& /dev/null
    # Fix uidcrc.exe bug.
    cp $EPOCROOT/epoc32/tools/uidcrc.exe `find $HOME/.wine -ipath '*windows/system32'`

    epatch "${FILESDIR}/installing_headers_should_not_require_building.patch"
}

src_configure() {
    symbianqtenv
    local conf_cmd
    if use amd64; then
        qtArch=linux-g++-64
    else
        qtArch=linux-g++
    fi
    # GCCE can't build webkit, but we still enable it so that one can use the precompiled library.
    conf_cmd="./configure -platform ${qtArch} -xplatform symbian/linux-gcce -prefix /usr/share/qt4/qt-symbian -confirm-license -opensource -fast -verbose -webkit"
    echo $conf_cmd
    $conf_cmd || die "Configure failed"
    echo CONFIG+=no_build >> .qmake.cache
}

src_install() {
    symbianqtenv

    dodir /usr/share/qt4/qt-symbian
    dodir /usr/share/qt4/qt-symbian/include

    cp -r mkspecs ${D}/usr/share/qt4/qt-symbian || die "Copy failed"
    # These need to be carried out with -j1, otherwise the nonatomic check for directory,
    # followed by creation of directory, might overlap and fail.
    for i in corelib xml xmlpatterns gui sql network svg script scripttools opengl multimedia testlib 3rdparty/webkit/WebCore; do
        emake INSTALL_ROOT="${D}" -j1 -C src/$i install_flat_headers install_class_headers install_targ_headers || die "emake install failed"
    done
    # A few exceptions not following the norm.
    emake INSTALL_ROOT="${D}" -j1 -C src/openvg install_flat_headers install_targ_headers || die "emake install failed"
    emake INSTALL_ROOT="${D}" -j1 -C src/phonon install_class_headers install_targ_headers || die "emake install failed"

    exeinto /usr/share/qt4/qt-symbian/bin
    for i in createpackage createpackage.pl elf2e32_qtwrapper moc patch_capabilities patch_capabilities.pl qmake rcc uic; do
        doexe ${S}/bin/$i || die "doexe failed"
        dosym ../share/qt4/qt-symbian/bin/$i /usr/bin/$i-symbian || die "dosym failed"
    done

    mkdir -p ${D}/usr/share/qt4/qt-symbian/src/s60installs || die "mkdir failed"
    cp src/s60installs/selfsigned.* ${D}/usr/share/qt4/qt-symbian/src/s60installs || die "cp failed"

    cd ..
    cp -r lib ${D}/usr/share/qt4/qt-symbian
}

#src_compile() {
#	true
#}

#src_install() {
#	cp -rv * "${D}"
#}
